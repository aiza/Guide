package fr.ranaivoson.guide;

/**
 * Created by Stagiaire on 11/10/2017.
 */

public class Constant {

    public static final String INTENT_TITLE = "INTENT_TITLE";

    public static final String INTENT_IS_RESTAURANT = "INTENT_IS_RESTAURANT";

    public static final String INTENT_OBJECT_RESTAURANT = "INTENT_OBJECT_RESTAURANT";
}
