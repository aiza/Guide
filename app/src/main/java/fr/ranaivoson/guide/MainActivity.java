package fr.ranaivoson.guide;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    // Déclaration de variables
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instance de classe Timer
        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                // TODO: lancement de l'écran HomeActivity
                Intent mainIntent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(mainIntent);
                finish();

            }
        }, 2000); // 2000 = durée en milliseconds


    }


}
