package fr.ranaivoson.guide;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.ranaivoson.guide.models.Restaurant;

/**
 * Created by Stagiaire on 11/10/2017.
 *
 * Class qui gère l'Adapter des restaurants
 *
 */


public class RestaurantAdapter extends ArrayAdapter<Restaurant>{


    LayoutInflater inflater;    // Variable affichage du layout item_restaurant
    int intResource;

    // Constructeur
    public RestaurantAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Restaurant> objects) {
        super(context, resource, objects);

        // Initialise inflater
        inflater = LayoutInflater.from(context);
        intResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        /**
         * Afficher R.layout.item_restaurant
         * Remplacement du convertView par le layout
         */
        convertView = inflater.inflate(intResource, null);

        // Récupère des éléments (titre et catégorie)
        TextView textViewTitle = convertView.findViewById(R.id.textViewTitle);
        TextView textViewCategory = convertView.findViewById(R.id.textViewCategory);

        textViewTitle.setText(getItem(position).getName());
        textViewCategory.setText(getItem(position).getCategory());

        return convertView;
        //return super.getView(position, convertView, parent);
    }

}
