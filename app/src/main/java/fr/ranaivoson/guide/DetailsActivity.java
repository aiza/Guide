package fr.ranaivoson.guide;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import fr.ranaivoson.guide.models.Restaurant;

public class DetailsActivity extends AppCompatActivity {

    private ImageView imageViewPhoto;
    private TextView textViewTitle;
    private TextView textViewCategory;
    private Button buttonPhone;
    private Button buttonMail;
    private Button buttonUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        imageViewPhoto = (ImageView) findViewById(R.id.imageViewPhoto);
        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewCategory = (TextView) findViewById(R.id.textViewCategory);
        buttonMail = (Button) findViewById(R.id.buttonMail);
        buttonPhone = (Button) findViewById(R.id.buttonPhone);
        buttonUrl = (Button) findViewById(R.id.buttonUrl);



        if (getIntent() != null) {

            // On récupère l'objet Restaurant
            Restaurant monRestaurant =  (Restaurant) getIntent().getExtras().get(Constant.INTENT_OBJECT_RESTAURANT);

            Picasso.with(DetailsActivity.this).load(monRestaurant.getImage()).into(imageViewPhoto);

            textViewTitle.setText(monRestaurant.getName());
            textViewCategory.setText(monRestaurant.getCategory());
            buttonMail.setText(monRestaurant.getMail());
            buttonPhone.setText(monRestaurant.getPhone());
            buttonUrl.setText(monRestaurant.getUrl());


        }

    }


    public void sendEmail(View view) {

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"recipient@example.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
        i.putExtra(Intent.EXTRA_TEXT   , "body of email");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(DetailsActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }

        startActivity(i);
    }

    public void callPhone(View view) {

        Intent intentCallPhone = new Intent(Intent.ACTION_DIAL,  Uri.parse(" Tél. " + buttonPhone.getText().toString()));
        startActivity(intentCallPhone);
    }

    public void webSite(View view) {

        // Redirection vers l'URL du buttonUrl
        Intent intentWebsite = new Intent(Intent.ACTION_VIEW, Uri.parse(buttonUrl.getText().toString()));
        startActivity(intentWebsite);
    }
}
