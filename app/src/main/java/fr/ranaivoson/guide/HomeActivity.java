package fr.ranaivoson.guide;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    private Button buttonRestaurant;
    private Button buttonHotel;
    private TextView textViewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        buttonRestaurant = (Button) findViewById(R.id.buttonRestaurant);
        buttonHotel = (Button) findViewById(R.id.buttonHotel);

        // TODO: ajouter un écouteur sur le click sur chaque button
        buttonRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(HomeActivity.this, ListingActivity.class);

                mainIntent.putExtra(Constant.INTENT_IS_RESTAURANT, true);
                mainIntent.putExtra(Constant.INTENT_TITLE, getString(R.string.listing_restaurant_title));

                startActivity(mainIntent);
            }
        });

        // TODO: ajouter un écouteur sur le click sur chaque button
        buttonHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(HomeActivity.this, ListingActivity.class);

                mainIntent.putExtra(Constant.INTENT_IS_RESTAURANT, false);
                mainIntent.putExtra(Constant.INTENT_TITLE, getString(R.string.listing_hotel_title));

                startActivity(mainIntent);
            }
        });

        // TODO: lancer l'écran suivant un click
        // TODO: gérer l'affichage du bon titre ("Les Restaurants", "Les Hotels")
    }
}
