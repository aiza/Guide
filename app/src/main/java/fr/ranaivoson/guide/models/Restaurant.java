package fr.ranaivoson.guide.models;

import java.io.Serializable;

/**
 * Created by Stagiaire on 11/10/2017.
 */

public class Restaurant implements Serializable {

    private String name;
    private String category;
    private String mail;
    private String phone;
    private String url;
    private String image;


    public Restaurant(String name, String category, String mail, String phone, String url, String image) {
        this.name = name;
        this.category = category;
        this.mail = mail;
        this.phone = phone;
        this.url = url;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getMail() {
        return mail;
    }

    public String getPhone() {
        return phone;
    }

    public String getUrl() {
        return url;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
