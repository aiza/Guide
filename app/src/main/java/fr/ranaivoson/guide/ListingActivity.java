package fr.ranaivoson.guide;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.ranaivoson.guide.models.Restaurant;

public class ListingActivity extends AppCompatActivity {

    private TextView textViewTitle;
    private GridView gridViewData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        gridViewData = (GridView) findViewById(R.id.gridViewData);

        // TODO: afficher le bon titre ("Les Restaurants", "Les Hotels")

        // Vérifier s'il y a eu un paramètre envoyé
        if (getIntent().getExtras() != null) {

            // Récupere du type d'etablissement (Restaurant ou Hotel)
            String title = getIntent().getExtras().getString(Constant.INTENT_TITLE);
            textViewTitle.setText(title);

            boolean isRestaurant = getIntent().getExtras().getBoolean(Constant.INTENT_IS_RESTAURANT);

            if (isRestaurant == true) {

                // Liste Restaurants
                final List<Restaurant> mesRestaurants = new ArrayList();
                mesRestaurants.add(new Restaurant("Le Paris", "Gastronomique", "info@leparis.fr", "021010101010", "https://www.mcdonalds.fr/", "http://2.bp.blogspot.com/-G20P9_nvoEE/Td5f_gow_UI/AAAAAAAABDY/V5-uC_IdzkY/s1600/macdo.jpg"));
                mesRestaurants.add(new Restaurant("La crémaillère", "Gastronomique", "info@lacremaillere.fr", "032020202020", "https://www.hotelsbarriere.com/", "http://speakenglishcenter.com/wp-content/uploads/2016/05/restaurant-939435_960_720.jpg"));
                mesRestaurants.add(new Restaurant("Le Paris", "Gastronomique", "info@leparis.fr", "021010101010", "https://leparis.fr", "http://monparisjoli.com/wp-content/uploads/2015/10/le-fouquets-825x510.jpg"));
                mesRestaurants.add(new Restaurant("La crémaillère", "Gastronomique", "info@lacremaillere.fr", "032020202020", "https://lacremaillere.fr", "https://loulou-paris.com/wp-content/uploads/2016/07/loulou-restaurant-paris-03-1000x800.jpg"));
                mesRestaurants.add(new Restaurant("Le Paris", "Gastronomique", "info@leparis.fr", "021010101010", "https://leparis.fr", "https://www.google.fr/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjtoKfPr-jWAhXBvRoKHcV_DaMQjRwIBw&url=https%3A%2F%2Fwww.pariszigzag.fr%2Fsortir-paris%2Frestaurants-paris-insolite%2Frestaurants-belle-epoque&psig=AOvVaw0KfzRJ2qUqWczu3lNOfZOt&ust=1507804495212815"));
                mesRestaurants.add(new Restaurant("La crémaillère", "Gastronomique", "info@lacremaillere.fr", "032020202020", "https://lacremaillere.fr", "https://loulou-paris.com/wp-content/uploads/2016/07/loulou-restaurant-paris-03-1000x800.jpg"));
                mesRestaurants.add(new Restaurant("Le Paris", "Gastronomique", "info@leparis.fr", "021010101010", "https://leparis.fr", "https://www.google.fr/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjtoKfPr-jWAhXBvRoKHcV_DaMQjRwIBw&url=https%3A%2F%2Fwww.pariszigzag.fr%2Fsortir-paris%2Frestaurants-paris-insolite%2Frestaurants-belle-epoque&psig=AOvVaw0KfzRJ2qUqWczu3lNOfZOt&ust=1507804495212815"));
                mesRestaurants.add(new Restaurant("La crémaillère", "Gastronomique", "info@lacremaillere.fr", "032020202020", "https://lacremaillere.fr", "https://loulou-paris.com/wp-content/uploads/2016/07/loulou-restaurant-paris-03-1000x800.jpg"));
                mesRestaurants.add(new Restaurant("Le Paris", "Gastronomique", "info@leparis.fr", "021010101010", "https://leparis.fr", "https://www.google.fr/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjtoKfPr-jWAhXBvRoKHcV_DaMQjRwIBw&url=https%3A%2F%2Fwww.pariszigzag.fr%2Fsortir-paris%2Frestaurants-paris-insolite%2Frestaurants-belle-epoque&psig=AOvVaw0KfzRJ2qUqWczu3lNOfZOt&ust=1507804495212815"));
                mesRestaurants.add(new Restaurant("La crémaillère", "Gastronomique", "info@lacremaillere.fr", "032020202020", "https://lacremaillere.fr", "https://loulou-paris.com/wp-content/uploads/2016/07/loulou-restaurant-paris-03-1000x800.jpg"));

                gridViewData.setAdapter(new RestaurantAdapter(ListingActivity.this, R.layout.item_restaurant, mesRestaurants));

                gridViewData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Intent mainIntent = new Intent(ListingActivity.this, DetailsActivity.class);

                        Bundle bundle = new Bundle();

                        bundle.putSerializable(Constant.INTENT_OBJECT_RESTAURANT, mesRestaurants.get(i));
                        mainIntent.putExtras(bundle);

                        startActivity(mainIntent);
                    }
                });

            } else {

                // Liste Hotels

            }

        }

        // TODO: afficher la liste des Restaurants

        // TODO: afficher la liste des Hotels
    }



}
